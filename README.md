# One Button Scan To Email

I had an old scanner lying around that I wanted to use for something. I also had a few MSP430 launchpads and Arduinos around the house. So I decided to create a quick one button to email scanner. The code is in 2 parts: micro-controller and scanner. There really isn't anything new here or original for that matter.  Just some python code that puts other open source projects to work.

## Scanner

The scanner uses python to pull it all together:  [pyserial](http://pyserial.sourceforge.net/), [SANE](http://www.sane-project.org/) and [ImageMagick](http://www.imagemagick.org).

Pyserial is used to communicate with the micro-controller  The python script simply waits for the micro-controller to send over data (any data) to the server and then starts the rest of the process

## uController

One button hooked up to the micro-controller that, when pressed, sends a serial signal to the server. That is really all it does. I used the [Energia IDE](http://energia.nu/) to program the MSP430.  

