#define led P1_0
#define button P1_4
#define easy P1_5

int scan = 0;
int debounce = 0;

void setup() {
  Serial.begin(2400);
  pinMode(led, OUTPUT);
  pinMode(easy, OUTPUT);
  digitalWrite(easy, LOW);
  digitalWrite(led, LOW);
  pinMode(button, INPUT);
  attachInterrupt(button, scan_request, RISING);
}

void loop() {
  if (scan > 0) {
    Serial.println("N");
    scan = 0;
  } else if (Serial.available() > 0) {
    while (Serial.available()) {
      Serial.read();
    }
    digitalWrite(easy, HIGH);
    delay(50);
    digitalWrite(easy, LOW);
  }
}

void scan_request() {
  if (millis() - debounce > 300) {
    scan = 1;
    debounce = millis();
  }
}
