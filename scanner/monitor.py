#!/usr/bin/python

from serial import Serial
from time import sleep
import subprocess
import sys
import os
import smtplib
import getpass
from email import encoders
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.MIMEText import MIMEText

def send_email():
	smtp_server = 'smtp.gmail.com'
	smtp_port = 587
	subject = 'Scanned Document'
	message = 'This was scanned for you.'
	
	msg = MIMEMultipart()
	msg['Subject'] = subject
	msg['To'] = recipient
	msg['From'] = sender
	part = MIMEText('text', "plain")
	part.set_payload(message)
	msg.attach(part)

	# add file
	att = MIMEBase('image', 'image')
	with open('onebutton.jpg', 'rb') as f:
		att.set_payload(f.read())
	encoders.encode_base64(att)
	att.add_header('Content-Disposition', 'attachment', filename='one_button_scan.jpg')
	msg.attach(att)

	# auth with gmail
	session = smtplib.SMTP(smtp_server, smtp_port)
	session.ehlo()
	session.starttls()
	session.ehlo()
	session.login(sender, pwd)

	# send message
	session.sendmail(sender, recipient, msg.as_string())

	session.quit()

	os.remove('onebutton.jpg')

def scan():
	with open('./onebutton', 'wb') as out:
		p = subprocess.Popen(['scanimage', '--resolution', '100'], stdout=out)
		p.wait()

	p = subprocess.Popen(['convert', 'onebutton', 'onebutton.jpg'])
	p.wait()

	os.remove('./onebutton')

if __name__ == "__main__":
	global pwd, sener, recipient 
	pwd = getpass.getpass()
	sender = raw_input("Sender email:")
	recipient = raw_input("Recipient email:")

	theport = "/dev/ttyACM0"
	s = Serial(port=theport, baudrate=2400)
	sleep(1)
	print "Ready to scan."
	try:
		while True:
			# read the input from micro controller; N + line feed
			a = s.read(3) 
			#print "Scanning..."
			scan()
			#print "Done scanning."
			#print "Sending email..."
			send_email()
			s.write("D")
			#print "Done sending email."
			
	except KeyboardInterrupt:
		pass
	s.close()
