#!/bin/bash


if [ -z $1 ]; then
	echo 'please supply a file name'
	exit 1
fi

# scan and covert to jpg
scanimage $1 && convert $1 $1.jpg

if [ -z $2 ] || [ $2 -ne 'n' ] ; then
	# remove high res image unless n arg is supplied
	rm $1
fi

# beep when done :)
echo -en "\007"
